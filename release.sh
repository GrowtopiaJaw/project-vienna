#! /bin/bash
echo -e "Please input a git tag name for this release"
echo -e "The git tag name for the previous release is '$(cat git-tag-name-previous.txt)'"
echo
read -p ": " git_tag_name
echo
if [ -z "$git_tag_name" ]; then
  echo -e "git tag name is empty! Using previous release's git tag name for this release..."
  echo
  git_tag_name="$(cat git-tag-name-previous.txt)"
else
  echo -e "Saving release details..."
  echo
  echo "$git_tag_name" > git-tag-name-previous.txt
fi
echo -e "Creating release..."
echo
if [ -z "$(ls *.part2*)" ]; then
hub release create -d -a "$(ls *.part1*)" -m "$(ls *.iso)

#### DETAILS
* **TechNet**: $(ls *.iso)
* **Digital River**: 
* **MD5**: $(cat md5.txt)
* **SHA1**: $(cat sha1.txt)
" "$git_tag_name"
else
hub release create -d -a "$(ls *.part1*)" -a "$(ls *.part2*)" -m "$(ls *.iso)

#### DETAILS
* **TechNet**: $(ls *.iso)
* **Digital River**: 
* **MD5**: $(cat md5.txt)
* **SHA1**: $(cat sha1.txt)
" "$git_tag_name"
fi
