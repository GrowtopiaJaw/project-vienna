#! /bin/bash
echo -e "Calculating MD5 hash sum for $(ls *.iso)..."
echo
md5sum "$(ls *.iso)" | awk '{ print $1 }' | tee md5.txt
echo -e "Calculating SHA1 hash sum for $(ls *.iso)..."
echo
sha1sum "$(ls *.iso)" | awk '{ print $1 }' | tee sha1.txt
echo -e "Creating RAR archive from $(ls *.iso)..."
echo
rar a -v2000000k "$(ls *.iso | sed 's/.iso//g')".rar "$(ls *.iso)"
